# 基于 Idux 组件库开发的业务组件库

## 安装

```shell
npm install @ixsu/components @ixsu/utils
```

## 全量引入

```ts
import '@ixsu/components/index.min.css'
import IxsComponents from '@ixsu/components'
// import '@ixsu/utils' // 工具函数

const app = createApp(App)
app.use(IxsComponents)
```

## 按需引入

```ts
import '@ixsu/components/button/style/index.css'
import IxsButton from '@ixsu/components/button'

const app = createApp(App)
app.use(IxsButton)
```

## 开发

```shell
npm install
npm run dev
```

## 参考

[Idux 组件库](https://idux.site/)
